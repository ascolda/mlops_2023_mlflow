Определение специализации врача на основе просмотренных документов в справочной системе Консилиум
==============================

## Учебный проект на курсе ODS MLOps
Проект выполнялся на основе реальной задачи: определить специальности врачей, пользователей справочной системы Консилиум, на основе их истории просмотренных документов. 
На основе определенных специальностей будут строиться кампании по персональным предложениям для пользователей. 

Сырые данные для обучения - названия материалов из справочной системы с проставленными тегами-специальностями врачей, на которых они ориентированы.
Например:
```
title: "Коклюш у детей: клиническая рекомендация"
tags: "врач-инфекционист; врач-невролог; врач-педиатр; врач-эпидемиолог; врач-пульмонолог"
```
Признаки для классификации строятся на основе tf-idf векторов, а метки классов -- названия специальностей. 

Используется SVM классификатор. Взвешенная f1 метрика = 0,94, что показывает хорошую разделимость классов.

Во время инференса, на вход модели поступают сконкатенированнеы названия материалов, на выходе -- топ5 специальностей с вероятностями классов.

```
[
    "39/1016 Протокол лечения анемии при хронической болезни почек. 39/1113 Протокол  лечения и профилактики хронического цистита у женщин. алгоритм первичной диагностики хбп. 9/855 Протокол ведения пациентов при ВИЧ-инфекции (обследование в целях установления диагноза и подготовки к лечению)",
    {
        "Нефролог": 0.3176,
        "Инфекционист": 0.1854,
        "Кардиолог": 0.1714,
        "Педиатр": 0.1662,
        "Стоматолог": 0.1593
    }
]
```

Project Organization
------------

    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   ├── clean_data.py
    │   │   └── concatenate_documents.py
    │   │   └── doctors_dict.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── lemmatize.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │       │                 predictions
    │       ├── predict_model.py
    │       ├── prepare_datasets.py
    │       └── train_model.py
    │   
    ├── pyproject.toml
    │
    ├── .gitlab-ci.yaml
    │
    ├── dvc.yaml
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
