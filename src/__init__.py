from src.data.clean_data import clean_data
from src.data.concatenate_documents import concatenate_documents
from src.features.lemmatize import lemmatize_data
from src.models.prepare_datasets import train_test_split_df
from src.models.train_model import train
from src.models.evaluate import evaluate