import click
import pandas as pd


def make_group(group: pd.DataFrame) -> pd.Series:
    """Для выбранной специализации группируем заголовки просмотренных документов по 5 штук"""
    shuffled_group = group.sample(frac=1, random_state=42)
    concatenated_documents = []
    for i in range(0, len(shuffled_group), 5):
        concatenated_documents.append(
            ". ".join(shuffled_group.iloc[i : i + 5]["Название документа"])
        )

    return pd.Series(
        concatenated_documents,
        index=[f"Object {i+1}" for i in range(len(concatenated_documents))],
    )


@click.command()
@click.argument(
    "input_path",
    type=click.Path(exists=True),
)
@click.argument(
    "output_path",
    type=click.Path(),
)
def concatenate_documents(input_path: str, output_path: str) -> None:
    """Функция группирует датафрейм по специализациям и формирует текстовые описания"""
    df = pd.read_csv(input_path, encoding="cp1251")
    df = (
        df.groupby("Врач")
        .apply(make_group)
        .reset_index()
        .drop("level_1", axis=1)
        .rename(columns={0: "Текст"})
    )
    df.to_csv(output_path, encoding="cp1251")
    print("Сохранил файл со сгруппированными данными")


if __name__ == "__main__":
    concatenate_documents()
