import html
import click
from string import punctuation

import nltk
import pandas as pd
import pymorphy2
import regex
from nltk.corpus import stopwords

morph = pymorphy2.MorphAnalyzer()
nltk.download("stopwords")
russian_stopwords = stopwords.words("russian")
blacklist = set(punctuation)
blacklist.update(["«", "»"])


def lemmatize_text(text: str) -> str:
    """returns a list, consisting of the lemmas from the input string"""
    text = html.unescape(text)
    text = text.replace("-", " ")
    text = text.replace("/", " ")
    text = regex.sub(r"[\s\xa0\u202F\uFEFF]+", " ", text)

    words = text.split()  # разбиваем текст на слова
    res = []
    for word in words:
        word = "".join(
            c for c in word if (c not in blacklist and c.isdigit() is False)
        )  # удаляем знаки препинания и цифры
        if (
            len(word) >= 3
        ):  # для всех слов, состоящих из трех и более букв, во множество ответов записываем нормальную форму слова
            word = morph.parse(word)[0].normal_form
            res.append(word)

    return (" ").join(res)


@click.command()
@click.argument(
    "input_path",
    type=click.Path(exists=True),
)
@click.argument(
    "output_path",
    type=click.Path(),
)
def lemmatize_data(input_path: str, output_path: str) -> None:
    """Функция лемматизирует тексты в датафрейме"""
    df = pd.read_csv(input_path, encoding="cp1251")
    df["Текст"] = df["Текст"].apply(lemmatize_text)
    df.to_csv(output_path, encoding="cp1251")
    print("Сохранил файл с лемматизированными данными для обучения")


if __name__ == "__main__":
    lemmatize_data()
