import click
import pandas as pd
from sklearn.model_selection import train_test_split


@click.command()
@click.argument(
    "input_path",
    type=click.Path(exists=True)
)
@click.argument(
    "output_path_train",
    type=click.Path()
)
@click.argument(
    "output_path_test",
    type=click.Path()
)
def train_test_split_df(
    input_path: str, output_path_train: str, output_path_test: str
) -> None:
    df = pd.read_csv(input_path, encoding="cp1251")
    train_data, test_data, train_labels, test_labels = train_test_split(
        df["Текст"],
        df["Врач"],
        test_size=0.2,
        random_state=42,
        shuffle=True,
        stratify=df["Врач"],
    )
    train_df = pd.concat([train_data, train_labels], axis=1)
    test_df = pd.concat([test_data, test_labels], axis=1)
    train_df.to_csv(output_path_train, encoding="cp1251", index=False)
    test_df.to_csv(output_path_test, encoding="cp1251", index=False)
    print("Сохранил обучающий и тестовый датасеты")


if __name__ == "__main__":
    train_test_split_df()