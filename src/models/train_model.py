import joblib
import click
import os
from typing import List
import pandas as pd
from nltk.corpus import stopwords

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.metrics import f1_score

import mlflow
from mlflow.models.signature import infer_signature 
from mlflow.tracking import MlflowClient 

from dotenv import load_dotenv
load_dotenv('.env')
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI") 

os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
os.environ['AWS_BUCKET_NAME'] = 'arts'
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000/"      


mlflow.set_tracking_uri(remote_server_uri) 
mlflow.set_experiment('doctors_svm_clf')

russian_stopwords = stopwords.words("russian")

@click.command()
@click.argument(
    "input_paths",
    type=click.Path(exists=True), nargs=2
)
@click.argument(
    "output_paths",
    type=click.Path(), nargs=2
)
def train(input_paths: List[str], output_paths: List[str]) -> None:
    with mlflow.start_run():
        mlflow.get_artifact_uri()
        train_df = pd.read_csv(input_paths[0], encoding="cp1251")
        test_df = pd.read_csv(input_paths[1], encoding="cp1251")
        train_df = train_df.rename(columns={'Врач':'doctor', "Текст":"text"})
        test_df = test_df.rename(columns={'Врач':'doctor', "Текст":"text"})

        y_train = train_df["doctor"]
        X_train = train_df.drop(columns=['doctor'])
        X_train = X_train.values.reshape(-1)

        y_test = test_df["doctor"]
        X_test = test_df.drop(columns=['doctor'])
        X_test = X_test.values.reshape(-1)

        svm_clf = Pipeline(
            [
                (
                    "tfidf",
                    TfidfVectorizer(
                        max_features=5000,
                        ngram_range=(1, 2),
                        stop_words=russian_stopwords,
                        analyzer="word",
                    ),
                ),
                ("clf", LinearSVC(C=5, class_weight="balanced", random_state=1)),  
            ]
        )

        svm_clf = svm_clf.fit(X_train, y_train)

        tfidf_params = svm_clf["tfidf"].get_params()
        clf_params = svm_clf["clf"].get_params()

        params = {**tfidf_params, **clf_params}
        del params['stop_words']
    
        joblib.dump(svm_clf, output_paths[0])
        print("Сохранил обученный svm классификатор")

        y_pred = svm_clf.predict(X_test)
        score = f1_score(list(y_test), y_pred, average="weighted")
        print("Посчитал f1 метрику:", round(score, 3))

        signature = infer_signature(X_test, y_test)
        
        mlflow.log_params(params) 
        mlflow.log_metric("f1", score)
        mlflow.sklearn.log_model(svm_clf, 'svm_model', signature=signature)

        score = pd.DataFrame([score])
        score.to_csv(output_paths[1], index=False)

        client = MlflowClient()
        experiment = dict(mlflow.get_experiment_by_name('doctors_svm_clf'))
        experiment_id = experiment['experiment_id']
        df = mlflow.search_runs([experiment_id])
        best_run_id = df.loc[0, 'run_id'] 
        print("best_run_id: ", best_run_id)

        

if __name__ == "__main__":
    train()
